name := """scalac"""
organization := "practical-task"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.8"
val circeVersion = "0.15.0-M1"

PlayKeys.devSettings := Seq("play.server.http.port" -> "8080")

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
libraryDependencies += "org.apache.flink" % "flink-core" % "1.14.3" % "provided"
libraryDependencies += "org.scalaj" %% "scalaj-http" % "2.4.2"
libraryDependencies += "org.apache.httpcomponents" % "httpcore" % "4.4.15"
libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5"


libraryDependencies ++= Seq(
  "io.circe"  %% "circe-core"     % circeVersion,
  "io.circe"  %% "circe-generic"  % circeVersion,
  "io.circe"  %% "circe-parser"   % circeVersion
)

