package controllers

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 *
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class GithubControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "GithubController GET" should {

    "render the organisation json from a new instance of controller" in {
      val controller = new GithubController(stubControllerComponents())
      val home = controller.index().apply(FakeRequest(GET, " /org/martins9436/contributors"))

      // status(home) mustBe OK
      // contentType(home) mustBe Some("text/html")
      // contentAsString(home) must include ("developer")
    }
  }
}
