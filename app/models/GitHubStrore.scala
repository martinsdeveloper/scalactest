package models

import scalaj.http.{Http, HttpOptions}
import io.circe.parser
import io.circe.generic.semiauto.deriveDecoder
import io.circe.{Decoder, Encoder, HCursor, Json}

import io.circe._, io.circe.generic.semiauto._, io.circe.parser._
import io.circe.Decoder.Result
import io.circe.generic.JsonCodec, io.circe.syntax._

import scala.collection.mutable.ListBuffer

object GitHubStrore {

    val baseURL = "https://api.github.com"

    case class DeveloperInfo(name: String, login: String, contributions: Int)
    case class RepoInfo(fullName: String)
    case class Commiter (name: String, email: String, date: String)
    case class CommitBody(commiterBody: String)

    var emailName = ListBuffer[(String,String)]()


    def getRepos(orgName:String, token: String): String = {
        val url = baseURL + f"/orgs/${orgName}/repos"
        // probably should have used ws and cats effect
        val httpResponse = Http(url).asString                                 

        implicit val decodeUser: Decoder[RepoInfo] = Decoder.forProduct1("full_name")(RepoInfo.apply _ )

        val decodeResult = parser.decode[List[RepoInfo]](httpResponse.body)

        var reposList: List[RepoInfo] =  decodeResult.getOrElse(List())

        implicit val developerInfoEncoder: Encoder[DeveloperInfo] =  Encoder.forProduct3("name", "login", "contributions")(d => (d.name, d.login, d.contributions ) )
        val developerList = for( (k,l) <- reposList.map(x => getCommits(x.fullName, token)).flatten.groupBy(x=> x)) yield DeveloperInfo(  emailName.filter(x=> x._2.compare(k)==0)(0)._1, k,l.length)


        // reposList.map(x => getCommits(x.fullName)).flatten.groupBy(x=> x)
        return "[{" + developerList.map(x => x.asJson(developerInfoEncoder)).toList.map(x=> f" \"developer\": $x").mkString(",").stripMargin.linesIterator.mkString("").trim + "}]"
    }

    def findCommiterEmail(httpResponse: String):  ListBuffer[String] = {
        val nodes = httpResponse.substring(1,httpResponse.length).split(",")

        val nodeLength:Int = nodes.length
        var returnList = ListBuffer[String]()
        for( i <- 0 to nodes.length-1) {
            if (nodes(i).contains("commit\":{\"author\":{\"name\":") ) {
                if(i+5 <= nodeLength){
                    emailName += ((nodes(i+3).substring(1, nodes(i+3).length-1).split(":\"").last, nodes(i+4).substring(0,nodes(i+4).length-1).split(":\"").last))
                    returnList += nodes(i+4).substring(0,nodes(i+4).length-1).split(":\"").last
                }
            }
        }
        return returnList

    }

    def getCommits(contributors: String, token: String):  ListBuffer[String] = {
        val commitsUrl = baseURL + f"/repos/${contributors}/commits"
        val httpResponse = GithubRequest.getAuthorized(commitsUrl, token)
        return findCommiterEmail(httpResponse)
    }
}
