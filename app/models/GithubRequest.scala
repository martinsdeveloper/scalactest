package models

import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.util.EntityUtils

import scalaj.http.{Http, HttpOptions}
import io.circe.parser
import io.circe.generic.semiauto.deriveDecoder
import io.circe.{Decoder, Encoder, HCursor, Json}

import io.circe._, io.circe.generic.semiauto._
import io.circe._, io.circe.parser._
import io.circe.Decoder.Result

object GithubRequest extends App {

    def getAuthorized(url: String, token: String ): String = {
        val httpGet = new HttpGet(url)

        httpGet.setHeader("Authorization", "token "+token)
        httpGet.setHeader("Content-Type", "application/json")

        val client = new DefaultHttpClient
        val responseEntitiy = client.execute(httpGet).getEntity()
        val content = EntityUtils.toString(responseEntitiy);  

        client.getConnectionManager.shutdown

        return content
    }
}
