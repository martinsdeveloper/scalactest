package controllers

import javax.inject._
import play.api._
import play.api.mvc._

import java.io._
import scala.xml.XML

import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.libs.json.Writes._
import play.api.libs.json.Json

import models.GitHubStrore
@Singleton
class GithubController @Inject()(val controllerComponents: ControllerComponents, configuration: play.api.Configuration) extends BaseController {
    def organisation(orgName:String) = Action { implicit request: Request[AnyContent] =>
      Ok(Json.parse(GitHubStrore.getRepos(orgName, configuration.underlying.getString("github.token"))))
    }
}
